# Change Log
v1.0.0 第一个版本

v1.0.3 增加项目说明

v1.1.0 去除debug输出

v1.1.2 调整目录结构，优化插件代码

v1.1.3 修复config加载错误

v1.1.4 更新说明文档，调整config的参数提示

v1.1.5 sequelize插件新支持开启动态更新表字段的属性

v1.1.6 移除request库，使用bent库替代

v1.1.7 增加log记录扩展

v1.1.9 增加redis链接集群支持。优化websocket服务模块

v1.2.0 1.迭代优化websocket模块，支持外部自定义协议IProtocol。
2.增加WsClient.

## [Unreleased]
- Initial release