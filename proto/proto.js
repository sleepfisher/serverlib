/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
"use strict";

var $protobuf = require("protobufjs/light");

var $root = ($protobuf.roots["default"] || ($protobuf.roots["default"] = new $protobuf.Root()))
.addJSON({
  framework: {
    nested: {
      MessageRequest: {
        oneofs: {
          _id: {
            oneof: [
              "id"
            ]
          },
          _data: {
            oneof: [
              "data"
            ]
          }
        },
        fields: {
          cmd: {
            rule: "required",
            type: "string",
            id: 1
          },
          id: {
            type: "int32",
            id: 2,
            options: {
              proto3_optional: true
            }
          },
          data: {
            type: "bytes",
            id: 3,
            options: {
              proto3_optional: true
            }
          }
        }
      },
      MessageResponse: {
        oneofs: {
          _id: {
            oneof: [
              "id"
            ]
          },
          _key: {
            oneof: [
              "key"
            ]
          },
          _data: {
            oneof: [
              "data"
            ]
          }
        },
        fields: {
          cmd: {
            rule: "required",
            type: "string",
            id: 1
          },
          id: {
            type: "int32",
            id: 2,
            options: {
              proto3_optional: true
            }
          },
          key: {
            type: "string",
            id: 3,
            options: {
              proto3_optional: true
            }
          },
          data: {
            type: "ResultInfo",
            id: 4,
            options: {
              proto3_optional: true
            }
          }
        }
      },
      ResultInfo: {
        oneofs: {
          _data: {
            oneof: [
              "data"
            ]
          },
          _msg: {
            oneof: [
              "msg"
            ]
          }
        },
        fields: {
          code: {
            rule: "required",
            type: "int32",
            id: 1
          },
          data: {
            type: "bytes",
            id: 2,
            options: {
              proto3_optional: true
            }
          },
          msg: {
            type: "string",
            id: 3,
            options: {
              proto3_optional: true
            }
          }
        }
      },
      MessageList: {
        values: {
          checkPlat: 0
        }
      }
    }
  },
  plat: {
    nested: {
      PlatInfoReq: {
        fields: {
          platid: {
            type: "int32",
            id: 1
          }
        }
      },
      PlatInfoRsp: {
        fields: {
          PlatID: {
            type: "int32",
            id: 1
          },
          Domain: {
            type: "string",
            id: 2
          },
          ClientVer: {
            type: "string",
            id: 3
          },
          LowVer: {
            type: "string",
            id: 4
          },
          ResVer: {
            type: "string",
            id: 5
          },
          Lan: {
            type: "string",
            id: 6
          },
          ActivityVer: {
            type: "string",
            id: 7
          }
        }
      }
    }
  }
});

module.exports = $root;
