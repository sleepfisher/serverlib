"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.App = exports.ClsUtils = exports.LogUtils = exports.TimeUtils = exports.FileUtils = exports.StringUtils = exports.HttpUtils = exports.WsBaseService = exports.PlusWsServer = exports.BaseError = exports.PlusSequelize = exports.PlusSchedule = exports.EnumCode = exports.BaseService = exports.processArg = exports.PlusRedis = exports.PlusHttpServer = exports.PlusMgr = void 0;
const path_1 = require("path");
const BaseEntity_1 = require("./entity/BaseEntity");
Object.defineProperty(exports, "BaseError", { enumerable: true, get: function () { return BaseEntity_1.BaseError; } });
const Code_1 = require("./entity/Code");
Object.defineProperty(exports, "EnumCode", { enumerable: true, get: function () { return Code_1.EnumCode; } });
const Config_1 = require("./entity/Config");
Object.defineProperty(exports, "processArg", { enumerable: true, get: function () { return Config_1.processArg; } });
const BaseService_1 = require("./plus/httpServer/BaseService");
Object.defineProperty(exports, "BaseService", { enumerable: true, get: function () { return BaseService_1.BaseService; } });
const PlusHttpServer_1 = require("./plus/PlusHttpServer");
Object.defineProperty(exports, "PlusHttpServer", { enumerable: true, get: function () { return PlusHttpServer_1.PlusHttpServer; } });
const PlusRedis_1 = require("./plus/PlusRedis");
Object.defineProperty(exports, "PlusRedis", { enumerable: true, get: function () { return PlusRedis_1.PlusRedis; } });
const PlusSchedule_1 = require("./plus/PlusSchedule");
Object.defineProperty(exports, "PlusSchedule", { enumerable: true, get: function () { return PlusSchedule_1.PlusSchedule; } });
const PlusSequelize_1 = require("./plus/PlusSequelize");
Object.defineProperty(exports, "PlusSequelize", { enumerable: true, get: function () { return PlusSequelize_1.PlusSequelize; } });
const PlusWsServer_1 = require("./plus/PlusWsServer");
Object.defineProperty(exports, "PlusWsServer", { enumerable: true, get: function () { return PlusWsServer_1.PlusWsServer; } });
const WsBaseService_1 = require("./plus/websocket/server/WsBaseService");
Object.defineProperty(exports, "WsBaseService", { enumerable: true, get: function () { return WsBaseService_1.WsBaseService; } });
const PlusMgr_1 = require("./PlusMgr");
Object.defineProperty(exports, "PlusMgr", { enumerable: true, get: function () { return PlusMgr_1.PlusMgr; } });
const ClsUtils_1 = require("./utils/ClsUtils");
Object.defineProperty(exports, "ClsUtils", { enumerable: true, get: function () { return ClsUtils_1.ClsUtils; } });
const FileUtils_1 = require("./utils/FileUtils");
Object.defineProperty(exports, "FileUtils", { enumerable: true, get: function () { return FileUtils_1.FileUtils; } });
const HttpUtils_1 = require("./utils/HttpUtils");
Object.defineProperty(exports, "HttpUtils", { enumerable: true, get: function () { return HttpUtils_1.HttpUtils; } });
const LogUtils_1 = require("./utils/LogUtils");
Object.defineProperty(exports, "LogUtils", { enumerable: true, get: function () { return LogUtils_1.LogUtils; } });
const StringUtils_1 = require("./utils/StringUtils");
Object.defineProperty(exports, "StringUtils", { enumerable: true, get: function () { return StringUtils_1.StringUtils; } });
const TimeUtils_1 = require("./utils/TimeUtils");
Object.defineProperty(exports, "TimeUtils", { enumerable: true, get: function () { return TimeUtils_1.TimeUtils; } });
class App {
    start() {
        let rootFile = process.argv[1];
        let rootDir = path_1.dirname(rootFile);
        global.rootPath = rootDir;
        let config = new Config_1.Config(Config_1.processArg.env, Config_1.processArg.main);
        global.log = new LogUtils_1.LogUtils(rootDir, "/opt/logs", config.logConfig);
        global.config = config;
        global.Debug = config.Debug;
        config.on("change", this.mConfigUpdate.bind(this));
        this.mConfigUpdate();
        this.handlerException();
        global.log.info("开始启动服务:" + JSON.stringify(Config_1.processArg));
        let plusMgr = new PlusMgr_1.PlusMgr();
        global.plus = plusMgr;
        plusMgr.on("complete", this.mOnComplete.bind(this));
        this.mInitPlus(plusMgr);
        plusMgr.start();
    }
    mInitPlus(plusMgr) {
        let config = global.config;
        if (config.sql)
            plusMgr.registPlus(new PlusSequelize_1.PlusSequelize(config.sql, global.rootPath + "/model"));
        plusMgr.registPlus(new PlusSchedule_1.PlusSchedule(global.rootPath + "/schedule"));
        if (config.redis)
            plusMgr.registPlus(new PlusRedis_1.PlusRedis(config.redis));
        let port = Config_1.processArg.port || config.port;
        if (port) {
            plusMgr.registPlus(new PlusHttpServer_1.PlusHttpServer(port, global.rootPath + "/controller"));
        }
        let wsPort = Config_1.processArg.wsPort || config.wsPort;
        if (wsPort) {
            plusMgr.registPlus(new PlusWsServer_1.PlusWsServer(PlusWsServer_1.PlusWsServer.NAME, wsPort, global.rootPath + "/wsController"));
        }
    }
    mOnComplete() {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    mConfigUpdate() {
        global.log.updateLogLevel(global.config.logLevels);
    }
    handlerException() {
        process.on('uncaughtException', function (err) {
            if (typeof err === 'object') {
                if (err.message) {
                    global.log.error('ERROR: ' + err.message);
                }
                if (err.stack) {
                    global.log.error(err.stack + " ERROR TIME: " + new Date());
                }
            }
            else {
                global.log.error('argument is not an object');
            }
            try {
                var killTimer = setTimeout(function () {
                    process.exit(1);
                }, 30000);
                killTimer.unref();
            }
            catch (e) {
                global.log.error('error when exit' + e.stack);
            }
        });
    }
}
exports.App = App;
//# sourceMappingURL=Index.js.map