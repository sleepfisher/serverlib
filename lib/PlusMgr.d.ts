/// <reference types="node" />
import { PlusBase } from "./plus/PlusBase";
import { EventEmitter } from "events";
export declare class PlusMgr extends EventEmitter {
    private _hash;
    private _list;
    constructor();
    registPlus(plus: PlusBase): void;
    start(): void;
    private _initNext;
    private _complete;
    getPlus(type: string): PlusBase;
    getPlus<T>(type: {
        NAME: string;
        new (...arg: any[]): T;
    }): T;
}
