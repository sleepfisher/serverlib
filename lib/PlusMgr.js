"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PlusMgr = void 0;
const events_1 = require("events");
class PlusMgr extends events_1.EventEmitter {
    constructor() {
        super();
        this._hash = {};
        this._list = [];
    }
    registPlus(plus) {
        if (!this._hash[plus.name]) {
            this._hash[plus.name] = plus;
            this._list.push(plus.name);
        }
    }
    start() {
        this._initNext();
    }
    _initNext() {
        if (this._list.length > 0) {
            let plus = this.getPlus(this._list.shift());
            plus.once("complete", this._complete.bind(this, plus.name));
            plus.init();
        }
        else {
            global.log.info("all plus init complete");
            this.emit("complete");
        }
    }
    _complete(plusName) {
        global.log.info(plusName + " init complete");
        this._initNext();
    }
    getPlus(...arg) {
        if (typeof arg[0] == "string") {
            return this._hash[arg[0]];
        }
        else if (arg[0].NAME) {
            return this._hash[arg[0].NAME];
        }
        return null;
    }
}
exports.PlusMgr = PlusMgr;
//# sourceMappingURL=PlusMgr.js.map