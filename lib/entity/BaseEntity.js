"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseError = exports.BaseResp = void 0;
class BaseResp {
    constructor(code, data, msg) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}
exports.BaseResp = BaseResp;
class BaseError extends Error {
    constructor(message, code = 0) {
        super(message);
        this._code = code;
    }
    get code() {
        return this._code;
    }
}
exports.BaseError = BaseError;
//# sourceMappingURL=BaseEntity.js.map