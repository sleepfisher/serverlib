"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.processArg = exports.Config = void 0;
const events_1 = require("events");
const fs_1 = require("fs");
const path_1 = require("path");
class Config extends events_1.EventEmitter {
    constructor(env = "dev", bMaster = "false") {
        super();
        this._env = env;
        this._bMaster = bMaster == "true";
        this._path = path_1.join(global.rootPath, './config_' + env + '.json');
        fs_1.watchFile(this._path, this._fileChange.bind(this));
        this._fileChange();
    }
    _fileChange() {
        let fileContent = fs_1.readFileSync(this._path).toString();
        try {
            let config = JSON.parse(fileContent);
            for (let key in config) {
                this[key] = config[key];
            }
            global.log && global.log.info("配置文件更新:\n" + fileContent);
            this.emit("change");
        }
        catch (e) {
            global.log && global.log.error(this._path + "配置文件" + e.message);
        }
    }
    get env() { return this._env; }
    get bMaster() { return this._bMaster; }
}
exports.Config = Config;
exports.processArg = {};
for (let i = 2; i < process.argv.length; i++) {
    let str = process.argv[i];
    str = str.substr(2);
    let arr = str.split("=");
    exports.processArg[arr[0]] = arr[1];
}
//# sourceMappingURL=Config.js.map