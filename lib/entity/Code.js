"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EnumCode = void 0;
var EnumCode;
(function (EnumCode) {
    EnumCode[EnumCode["SUCCESS"] = 200] = "SUCCESS";
    EnumCode[EnumCode["FAIL"] = 500] = "FAIL";
    EnumCode[EnumCode["SQL_ERROR"] = 100001] = "SQL_ERROR";
    EnumCode[EnumCode["REMOTE_ERROR"] = 100002] = "REMOTE_ERROR";
    EnumCode[EnumCode["PARAM_MISSING"] = 100003] = "PARAM_MISSING";
    EnumCode[EnumCode["PARAM_INVALID"] = 100004] = "PARAM_INVALID";
})(EnumCode = exports.EnumCode || (exports.EnumCode = {}));
//# sourceMappingURL=Code.js.map