/// <reference types="node" />
/// <reference types="mocha" />
import Global = NodeJS.Global;
import { PlusMgr } from "../PlusMgr";
import { LogUtils } from "../utils/LogUtils";
import { IConfig } from "./IConfig";
export interface IGlobal extends Global {
    config: IConfig;
    plus: PlusMgr;
    log: LogUtils;
    rootPath: string;
    Debug: boolean;
}
