/// <reference types="node" />
import { EventEmitter } from "events";
import { ISqlConfig, IRedisOption, EnumLogLevel } from "./IConfig";
export declare class Config extends EventEmitter {
    sql: ISqlConfig;
    tokenServer?: string;
    port: number;
    redis: IRedisOption;
    logLevels: {
        [key: string]: EnumLogLevel;
    };
    private _path;
    private _env;
    private _bMaster;
    constructor(env?: string, bMaster?: string);
    private _fileChange;
    get env(): string;
    get bMaster(): boolean;
}
export declare var processArg: any;
