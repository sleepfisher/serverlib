export declare class BaseResp {
    code: number;
    msg: string;
    data: any;
    constructor(code: number, data?: any, msg?: string);
}
export declare class BaseError extends Error {
    private _code;
    constructor(message: string, code?: number);
    get code(): number;
}
