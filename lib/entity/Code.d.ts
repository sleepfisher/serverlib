export declare enum EnumCode {
    SUCCESS = 200,
    FAIL = 500,
    SQL_ERROR = 100001,
    REMOTE_ERROR = 100002,
    PARAM_MISSING = 100003,
    PARAM_INVALID = 100004
}
