/// <reference types="node" />
import { EventEmitter } from "events";
import { Configuration } from "log4js";
export interface IConfig extends EventEmitter {
    sql: ISqlConfig;
    port: number;
    wsPort: number;
    redis: IRedisOption;
    bMaster: boolean;
    env: string;
    logLevels: {
        [key: string]: EnumLogLevel;
    };
    logConfig?: Configuration;
    koa?: {
        proxy?: boolean;
        subdomainOffset?: number;
        proxyIpHeader?: string;
        maxIpsCount?: number;
        env?: any;
        keys?: any;
    };
}
export interface ISqlConfig {
    dbName: string;
    user: string;
    pwd: string;
    options: {
        host: string;
        port: number;
        dialect: string;
    };
}
export interface IRedisOption {
    port: number;
    host: string;
    password?: string;
    db: number;
    cluster?: [{
        host: string;
        port: number;
    }];
}
export declare const enum EnumLogLevel {
    ALL = "ALL",
    TRACE = "TRACE",
    DEBUG = "DEBUG",
    INFO = "INFO",
    WARN = "WARN",
    ERROR = "ERROR",
    FATAL = "FATAL",
    MARK = "MARK",
    OFF = "OFF"
}
