"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Op = void 0;
const OpContent = {
    ne: Symbol.for('ne'),
    gte: Symbol.for('gte'),
    gt: Symbol.for('gt'),
    lte: Symbol.for('lte'),
    lt: Symbol.for('lt'),
    notBetween: Symbol.for('notBetween'),
    between: Symbol.for("between")
};
exports.Op = OpContent;
//# sourceMappingURL=Operators.js.map