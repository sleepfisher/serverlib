import { WhereValue } from "./HashMap";
import { Op } from "./Operators";
export declare class OpFunc {
    private static _inst;
    static get inst(): OpFunc;
    private _hash;
    constructor();
    private _registFunc;
    check<T>(funcName: symbol, value: WhereValue, currValue: WhereValue): boolean;
    private _gtFunc;
    private _gteFunc;
    private _betweenFunc;
    private _ltFunc;
    private _lteFunc;
    private _neFunc;
    private _notBetweenFunc;
}
export interface WhereOperators {
    [Op.gt]?: number | string;
    [Op.gte]?: number | string;
    [Op.lt]?: number | string;
    [Op.lte]?: number | string;
    [Op.between]?: Array<number | string>;
    [Op.notBetween]?: Array<number | string>;
    [Op.ne]?: number | string;
}
