interface IOp {
    readonly ne: unique symbol;
    readonly gte: unique symbol;
    readonly gt: unique symbol;
    readonly lte: unique symbol;
    readonly lt: unique symbol;
    readonly notBetween: unique symbol;
    readonly between: unique symbol;
}
export declare const Op: IOp;
export {};
