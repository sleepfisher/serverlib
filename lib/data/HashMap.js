"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.lowCpData = exports.HashMap = void 0;
const OpFunc_1 = require("./OpFunc");
class HashMap {
    constructor(cls) {
        this._values = [];
        this._indexHash = {};
        this._cls = cls;
    }
    _installT(d) {
        if (this._cls) {
            var newD = new this._cls();
            lowCpData(newD, d);
            return newD;
        }
        return d;
    }
    add(key, val) {
        if (this._indexHash[key] == null) {
            val = this._installT(val);
            this._values.push(val);
            this._indexHash[key] = this._values.length - 1;
            return true;
        }
        else {
            let index = this._indexHash[key];
            if (this._cls) {
                lowCpData(this._values[index], val);
            }
            else {
                this._values[index] = val;
            }
            return false;
        }
    }
    get(key) {
        let index = this._indexHash[key];
        return this._values[index];
    }
    del(key) {
        var index = this._indexHash[key];
        if (index == null) {
            return null;
        }
        let value = this._values.splice(index, 1)[0];
        this._clearIndex(index);
        return value;
    }
    getAt(index) {
        return this._values[index];
    }
    delAt(index) {
        if (index >= this._values.length)
            return null;
        let value = this._values[index];
        this._values.splice(index, 1);
        this._clearIndex(index);
        return value;
    }
    _clearIndex(index) {
        for (let key in this._indexHash) {
            let currIndex = this._indexHash[key];
            if (currIndex > index) {
                this._indexHash[key] -= 1;
            }
            else if (currIndex == index) {
                delete this._indexHash[key];
            }
        }
    }
    findOne(condition) {
        for (var i = 0, len = this._values.length; i < len; i++) {
            if (this._checkCondition(this._values[i], condition))
                return this._values[i];
        }
        return null;
    }
    find(condition) {
        return this._values.filter((v) => {
            return this._checkCondition(v, condition);
        });
    }
    findOneByMethod(method) {
        for (var i = 0, len = this._values.length; i < len; i++) {
            if (method(this._values[i]))
                return this._values[i];
        }
        return null;
    }
    findByMethod(method) {
        return this._values.filter((v) => {
            return method(v);
        });
    }
    foreach(method) {
        for (let i = 0, len = this._values.length; i < len; i++) {
            method(this._values[i]);
        }
    }
    _checkCondition(d, condition) {
        for (var key in condition) {
            let child = condition[key];
            if (child instanceof Array) {
                if (child.indexOf(d[key]) == -1)
                    return false;
            }
            else {
                let type = typeof child;
                if (type == "number" || type == "string" || type == "boolean") {
                    if (child != d[key])
                        return false;
                }
                else {
                    let ops = Object.getOwnPropertySymbols(child);
                    for (let i = 0, len = ops.length; i < len; i++) {
                        if (!OpFunc_1.OpFunc.inst.check(ops[i], d[key], child[ops[i]]))
                            return false;
                    }
                }
            }
            ;
        }
        return true;
    }
    has(key) {
        return this._indexHash[key] != null;
    }
    get length() {
        return this._values.length;
    }
    get values() {
        return this._values.concat();
    }
    dispose() {
        this._values = null;
        this._indexHash = null;
    }
}
exports.HashMap = HashMap;
function lowCpData(target, data, chkTargetKey = false) {
    for (var key in data) {
        if (data.hasOwnProperty(key)) {
            var element = data[key];
            if (element == null)
                continue;
            if ((chkTargetKey && target.hasOwnProperty(key)) || !chkTargetKey) {
                target[key] = element;
            }
        }
    }
}
exports.lowCpData = lowCpData;
//# sourceMappingURL=HashMap.js.map