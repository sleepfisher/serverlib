import { WhereOperators } from "./OpFunc";
export declare class HashMap<T> {
    private _values;
    private _indexHash;
    protected _cls: any;
    constructor(cls?: any);
    protected _installT(d: T): T;
    add(key: string | number, val: T): boolean;
    get(key: string | number): T;
    del(key: string | number): T;
    getAt(index: number): T;
    delAt(index: number): T;
    private _clearIndex;
    findOne(condition: WhereOption<T>): T;
    find(condition: WhereOption<T>): T[];
    findOneByMethod(method: compareFunc<T>): T;
    findByMethod(method: compareFunc<T>): T[];
    foreach(method: (d: T) => void): void;
    private _checkCondition;
    has(key: string | number): boolean;
    get length(): number;
    get values(): Array<T>;
    dispose(): void;
}
export declare function lowCpData(target: any, data: any, chkTargetKey?: boolean): void;
export declare type WhereOption<T> = {
    [P in keyof T]?: WhereValue;
};
export declare type WhereValue = number | string | boolean | Array<number | string | boolean> | WhereOperators;
export declare type compareFunc<T> = (item: T) => boolean;
