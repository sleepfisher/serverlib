"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OpFunc = void 0;
const Operators_1 = require("./Operators");
class OpFunc {
    constructor() {
        this._hash = {};
        this._registFunc(Operators_1.Op.gt, this._gtFunc);
        this._registFunc(Operators_1.Op.gte, this._gteFunc);
        this._registFunc(Operators_1.Op.between, this._betweenFunc);
        this._registFunc(Operators_1.Op.lt, this._ltFunc);
        this._registFunc(Operators_1.Op.lte, this._lteFunc);
        this._registFunc(Operators_1.Op.ne, this._neFunc);
        this._registFunc(Operators_1.Op.notBetween, this._notBetweenFunc);
    }
    static get inst() { return this._inst || new OpFunc(); }
    _registFunc(funcName, func) {
        this._hash[funcName] = func;
    }
    check(funcName, value, currValue) {
        let func = this._hash[funcName];
        if (func) {
            return func.call(this, value, currValue);
        }
        return true;
    }
    _gtFunc(value, currValue) {
        return value > currValue;
    }
    _gteFunc(value, currValue) {
        return value >= currValue;
    }
    _betweenFunc(value, currValue) {
        return value >= currValue[0] && value <= currValue[1];
    }
    _ltFunc(value, currValue) {
        return value < currValue;
    }
    _lteFunc(value, currValue) {
        return value <= currValue;
    }
    _neFunc(value, currValue) {
        return value != currValue;
    }
    _notBetweenFunc(value, currValue) {
        return value <= currValue[0] || value >= currValue[1];
    }
}
exports.OpFunc = OpFunc;
//# sourceMappingURL=OpFunc.js.map