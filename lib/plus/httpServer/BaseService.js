"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RegistMethodVo = exports.BaseService = void 0;
const BaseEntity_1 = require("../../entity/BaseEntity");
const Code_1 = require("../../entity/Code");
const PlusRedis_1 = require("../PlusRedis");
class BaseService {
    constructor() {
        this._methodList = [];
    }
    _regist(func, method, uri) {
        var str = func.toString().split("\r\n")[0];
        var arr;
        if (str.indexOf("(") != -1 && str.indexOf(")") != -1) {
            str = str.split("(")[1].split(")")[0];
            str = str.replace(/\s+/g, "");
            arr = str.split(",");
            for (var i = 0; i < arr.length; i++) {
                var dengIndex = arr[i].indexOf("=");
                if (dengIndex != -1) {
                    arr[i] = String(arr[i]).substring(0, dengIndex);
                }
            }
        }
        let methodVo = new RegistMethodVo(this, func, func.name, method, ...arr);
        methodVo.uri = uri;
        this._methodList.push(methodVo);
    }
    throwError(message, code) {
        throw new BaseEntity_1.BaseError(message, code);
    }
    get methods() {
        return this._methodList;
    }
    get log() {
        return global.log;
    }
    get redis() {
        return global.plus.getPlus(PlusRedis_1.PlusRedis).client;
    }
}
exports.BaseService = BaseService;
class RegistMethodVo {
    constructor(caller, func, name, method, ...args) {
        this.execute = (ctx) => __awaiter(this, void 0, void 0, function* () {
            var arg = [];
            var data = this.method == "post" ? ctx.request.body : ctx.request.query;
            for (var i = 0; data && this.args && i < this.args.length; i++) {
                arg.push(data[this.args[i]]);
            }
            arg.push(ctx);
            try {
                let time = Date.now();
                var result = yield this.func.call(this.caller, ...arg);
                ctx.body = new BaseEntity_1.BaseResp(200, result);
                global.log.trace("[" + this.name + "]耗时" + (Date.now() - time) + "ms");
            }
            catch (err) {
                global.log.error("失败" + this.name + "----" + (data ? JSON.stringify(data) : "") + "\r\n" + err.stack);
                ctx.body = new BaseEntity_1.BaseResp(err.code || Code_1.EnumCode.FAIL, err.message);
            }
        });
        this.func = func;
        this.name = name;
        this.args = args;
        this.caller = caller;
        this.method = method;
    }
}
exports.RegistMethodVo = RegistMethodVo;
//# sourceMappingURL=BaseService.js.map