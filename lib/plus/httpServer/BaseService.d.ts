/// <reference types="ioredis" />
export declare class BaseService {
    private _methodList;
    constructor();
    protected _regist(func: Function, method: string, uri?: string): void;
    throwError(message: string, code?: number): void;
    get methods(): RegistMethodVo[];
    get log(): import("../../Index").LogUtils;
    get redis(): import("ioredis").Redis;
}
export declare class RegistMethodVo {
    func: Function;
    name: string;
    args: Array<any>;
    caller: any;
    method: string;
    uri: string;
    constructor(caller: any, func: Function, name: string, method: string, ...args: any[]);
    execute: (ctx: any) => Promise<void>;
}
