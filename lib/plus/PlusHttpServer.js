"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PlusHttpServer = void 0;
const PlusBase_1 = require("./PlusBase");
const Koa = require("koa");
const cors = require("koa2-cors");
const router = require("koa-router")();
const http = require("http");
const path_1 = require("path");
const fs_1 = require("fs");
const BaseService_1 = require("./httpServer/BaseService");
const ClsUtils_1 = require("../utils/ClsUtils");
const bodyParser = require('koa-bodyparser');
class PlusHttpServer extends PlusBase_1.PlusBase {
    constructor(port, path) {
        super(PlusHttpServer.NAME);
        this._port = port;
        this._path = path;
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            var app = new Koa(global.config.koa);
            var server = http.createServer(app.callback());
            app.use(cors());
            app.use(bodyParser());
            var files = fs_1.readdirSync(this._path);
            var js_files = files.filter((f) => {
                return f.endsWith('.js');
            });
            for (var f of js_files) {
                let mapping = require(path_1.join(this._path, f));
                for (var key in mapping) {
                    if (ClsUtils_1.ClsUtils.targetIsCls(mapping[key], BaseService_1.BaseService)) {
                        var service = new mapping[key]();
                        for (var i = 0; i < service.methods.length; i++) {
                            let methodVo = service.methods[i];
                            let url = "/" + (methodVo.uri || (key + "/" + methodVo.name));
                            if (methodVo.method.toLocaleLowerCase() == "post") {
                                router.post(url, methodVo.execute);
                            }
                            else if (methodVo.method.toLocaleLowerCase() == "get") {
                                router.get(url, methodVo.execute);
                            }
                        }
                    }
                }
            }
            app.on('error', (err, ctx) => {
                this.log.error(`请求错误:\n来源ip:${ctx.ip}\n请求连接:${ctx.href}\n参数:${err.body}\n错误信息:${err.stack}`);
            });
            app.use(router.routes());
            server.listen(this._port);
            this.mCompleteHandle();
        });
    }
}
exports.PlusHttpServer = PlusHttpServer;
PlusHttpServer.NAME = "httpServer";
//# sourceMappingURL=PlusHttpServer.js.map