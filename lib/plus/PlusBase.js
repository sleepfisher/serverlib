"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PlusBase = void 0;
const events_1 = require("events");
class PlusBase extends events_1.EventEmitter {
    constructor(name) {
        super();
        this._name = name;
    }
    mCompleteHandle() {
        this.emit("complete");
    }
    mOnFail(err) {
        this.log.error(`模块[${this._name}]初始化失败`, err);
    }
    get name() {
        return this._name;
    }
    get log() {
        return global.log;
    }
}
exports.PlusBase = PlusBase;
//# sourceMappingURL=PlusBase.js.map