"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PlusWsServer = void 0;
const PlusBase_1 = require("./PlusBase");
const WsServer_1 = require("./websocket/server/WsServer");
class PlusWsServer extends PlusBase_1.PlusBase {
    constructor(name, port, path, protocol, isProxy = false) {
        super(name);
        this._isProxy = isProxy;
        this._ws = new WsServer_1.WsServer(port, path, protocol, isProxy);
    }
    init() {
        this._ws.verifyFunc = this._verifyFunc.bind(this);
        this._ws.on("closeClient", this._onCloseClient.bind(this));
        this._ws.on("message", this._onMessage.bind(this));
        this._ws.init();
        this.mCompleteHandle();
    }
    _verifyFunc(info) {
        return true;
    }
    getClient(link) {
        return this._ws.getClient(link);
    }
    getClientByKey(key) {
        return this._ws.getClientByKey(key);
    }
    closeClient(linkId) {
        this._ws.closeClient(linkId);
    }
    send(linkId, data) {
        this._ws.send(linkId, data);
    }
    sendCmd(linkId, cmd, data, id = 0) {
        this._ws.sendCmd(linkId, cmd, data, id);
    }
    _onCloseClient(linkId) {
    }
    _onMessage(linkId, data) {
    }
}
exports.PlusWsServer = PlusWsServer;
PlusWsServer.NAME = "PlusWsServer";
//# sourceMappingURL=PlusWsServer.js.map