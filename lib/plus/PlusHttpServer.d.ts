import { PlusBase } from "./PlusBase";
export declare class PlusHttpServer extends PlusBase {
    static NAME: string;
    private _port;
    private _path;
    constructor(port: number, path: string);
    init(): Promise<void>;
}
