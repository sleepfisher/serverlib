import { PlusBase } from "./PlusBase";
import { IProtocol } from "./websocket/protocol/IProtocol";
import { SocketClient } from "./websocket/server/SocketClient";
import { WsServer } from "./websocket/server/WsServer";
export declare class PlusWsServer extends PlusBase {
    static NAME: string;
    protected _ws: WsServer;
    protected _isProxy: boolean;
    constructor(name: string, port: number, path: string, protocol?: IProtocol, isProxy?: boolean);
    init(): void;
    protected _verifyFunc(info: any): boolean;
    getClient(link: string): SocketClient;
    getClientByKey(key: string): SocketClient;
    closeClient(linkId: string): void;
    send(linkId: string, data: any): void;
    sendCmd(linkId: string, cmd: string, data: any, id?: number): void;
    protected _onCloseClient(linkId: string): void;
    protected _onMessage(linkId: string, data: any): void;
}
