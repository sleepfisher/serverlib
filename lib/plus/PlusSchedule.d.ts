import { PlusBase } from "./PlusBase";
import { ScheduleBase } from "./schedule/ScheduleBase";
export declare class PlusSchedule extends PlusBase {
    static NAME: string;
    private _path;
    constructor(path: string);
    init(): Promise<void>;
    regisSchedule(scheduleItem: ScheduleBase): Promise<void>;
}
