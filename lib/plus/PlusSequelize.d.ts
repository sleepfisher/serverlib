import { Options, Sequelize } from "sequelize";
import { PlusBase } from "./PlusBase";
import { ModelBase } from "./sequelize/ModelBase";
import { ModelConfigBase } from "./sequelize/ModelConfigBase";
import { ISqlConfig } from "../entity/IConfig";
export declare class PlusSequelize extends PlusBase {
    static NAME: string;
    protected _sequelize: Sequelize;
    protected mModelHash: any;
    protected mModelPath: string;
    private _option;
    constructor(config: ISqlConfig, modelPath?: string, options?: Options, name?: string);
    init(): Promise<void>;
    protected mInitModelPath(): Promise<void>;
    initModel(config: ModelConfigBase, modelCls: typeof ModelBase): Promise<void>;
    defineModel<T, T1>(config: ModelConfigBase): typeof import("sequelize/types").Model;
    getModel(name: string): any;
    protected mGetValues(): {};
    _onLog(sql: string, time: number): void;
}
