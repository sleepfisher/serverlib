"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ModelBase = void 0;
const sequelize_1 = require("sequelize");
class ModelBase extends sequelize_1.Model {
    getValues() {
        var arr = this["_options"].attributes;
        var obj = {};
        if (arr) {
            for (var i = 0; i < arr.length; i++) {
                obj[arr[i]] = this[arr[i]];
            }
        }
        else {
            for (var key in this["dataValues"]) {
                obj[key] = this[key];
            }
        }
        return obj;
    }
}
exports.ModelBase = ModelBase;
//# sourceMappingURL=ModelBase.js.map