import { DataTypeAbstract, ModelAttributeColumnOptions, StringDataType } from "sequelize";
export declare enum EnumChangeType {
    none = 0,
    numToStr = 1,
    json = 2,
    time = 3
}
export declare class ModelConfigBase {
    static readonly TABLE_NAME: string;
    protected mName: string;
    protected mAttr: any;
    protected mNeedSync: boolean;
    protected mForce: boolean;
    protected mAlter: boolean;
    protected mHooks: any;
    private _indexs;
    constructor(name: string);
    init(): void;
    protected mInit(): void;
    protected mAddAttr(name: string, val: string | StringDataType | DataTypeAbstract | ModelAttributeColumnOptions, changeType?: EnumChangeType): void;
    protected mAddIndexes(name: string, fields: string[], type?: string, method?: string): void;
    protected mAddHook<T>(type: string, fn: (res: T) => void): void;
    getChangeFunc(key: string, changeType: EnumChangeType): () => any;
    setChangeFunc(key: string, changeType: EnumChangeType): (val: any) => void;
    setNumToStr(key: string): Function;
    get name(): string;
    get attr(): any;
    get needSync(): boolean;
    get force(): boolean;
    get alter(): boolean;
    get hooks(): any;
    get indexes(): any[];
    get options(): any;
}
