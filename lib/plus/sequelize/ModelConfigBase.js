"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ModelConfigBase = exports.EnumChangeType = void 0;
const sequelize_1 = require("sequelize");
var EnumChangeType;
(function (EnumChangeType) {
    EnumChangeType[EnumChangeType["none"] = 0] = "none";
    EnumChangeType[EnumChangeType["numToStr"] = 1] = "numToStr";
    EnumChangeType[EnumChangeType["json"] = 2] = "json";
    EnumChangeType[EnumChangeType["time"] = 3] = "time";
})(EnumChangeType = exports.EnumChangeType || (exports.EnumChangeType = {}));
class ModelConfigBase {
    constructor(name) {
        this.mNeedSync = true;
        this.mForce = false;
        this.mAlter = false;
        this.mAttr = {};
        this.mHooks = {};
        this._indexs = [];
        this.mName = name;
    }
    init() {
        this.mInit();
    }
    mInit() {
    }
    mAddAttr(name, val, changeType = EnumChangeType.none) {
        if (val == sequelize_1.DataTypes.DATE) {
            changeType = EnumChangeType.time;
        }
        if (!val['type']) {
            var newVal = { type: val };
        }
        else {
            var newVal = val;
        }
        if (changeType != EnumChangeType.none) {
            newVal.get = this.getChangeFunc(name, changeType);
            newVal.set = this.setChangeFunc(name, changeType);
        }
        this.mAttr[name] = newVal;
    }
    mAddIndexes(name, fields, type = "Normal", method = "BTREE") {
        if (type == "Normal") {
            this._indexs.push({ name: this.name + "_" + name, method: method, fields: fields });
        }
        else {
            this._indexs.push({ name: this.name + "_" + name, method: method, type: type, fields: fields });
        }
    }
    mAddHook(type, fn) {
        this.mHooks[type] = fn;
    }
    getChangeFunc(key, changeType) {
        if (changeType == EnumChangeType.numToStr) {
            return function () {
                return Number(this.getDataValue(key));
            };
        }
        else if (changeType == EnumChangeType.json) {
            return function () {
                var value = this.getDataValue(key);
                try {
                    return value ? JSON.parse(value) : null;
                }
                catch (_a) {
                    return null;
                }
            };
        }
        else if (changeType == EnumChangeType.time) {
            return function () {
                var value = this.getDataValue(key);
                return value ? value.getTime() : null;
            };
        }
    }
    setChangeFunc(key, changeType) {
        if (changeType == EnumChangeType.numToStr) {
            return function (value) {
                this.setDataValue(key, value.toString());
            };
        }
        else if (changeType == EnumChangeType.json) {
            return function (value) {
                try {
                    if (typeof value != "string") {
                        value = value ? JSON.stringify(value) : "";
                    }
                }
                catch (e) {
                }
                this.setDataValue(key, value);
            };
        }
    }
    setNumToStr(key) {
        return function (value) {
            this.setDataValue(key, value.toString());
        };
    }
    get name() {
        return this.mName;
    }
    get attr() {
        var attrs = {};
        for (let key in this.mAttr) {
            let value = this.mAttr[key];
            if (typeof value === 'object' && value['type']) {
                attrs[key] = value;
            }
            else {
                attrs[key] = {
                    type: value,
                };
            }
        }
        return attrs;
    }
    get needSync() {
        return this.mNeedSync;
    }
    get force() {
        return this.mForce;
    }
    get alter() {
        return this.mAlter;
    }
    get hooks() {
        return this.mHooks;
    }
    get indexes() {
        return this._indexs;
    }
    get options() {
        var options = {
            tableName: this.name,
            modelName: this.name,
            timestamps: true,
            indexes: this.indexes,
            underscored: true,
            hooks: {}
        };
        for (var key in this.hooks) {
            options.hooks[key] = this.hooks[key];
        }
        return options;
    }
}
exports.ModelConfigBase = ModelConfigBase;
//# sourceMappingURL=ModelConfigBase.js.map