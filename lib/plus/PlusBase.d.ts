/// <reference types="node" />
import { EventEmitter } from "events";
export declare abstract class PlusBase extends EventEmitter {
    private _name;
    constructor(name: string);
    abstract init(): void;
    protected mCompleteHandle(): void;
    protected mOnFail(err?: any): void;
    get name(): string;
    get log(): import("../Index").LogUtils;
}
