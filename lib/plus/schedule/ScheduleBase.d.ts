export declare abstract class ScheduleBase {
    private _isStart;
    abstract get config(): IScheduleConfig;
    protected abstract mExecute(): any;
    private _scheduleObj;
    private _timer;
    start(): Promise<void>;
    execute(): Promise<void>;
    stop(): void;
    get log(): import("../../Index").LogUtils;
}
export interface IScheduleConfig {
    interval?: string;
    cron?: string;
    type?: string;
    justRun?: boolean;
    close?: boolean;
    master?: boolean;
}
