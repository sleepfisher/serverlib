"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ScheduleBase = void 0;
const timers_1 = require("timers");
const Index_1 = require("../../Index");
const schedule = require('node-schedule');
class ScheduleBase {
    constructor() {
        this._isStart = false;
    }
    start() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this._isStart)
                return;
            if (this.config.close)
                return;
            this._isStart = true;
            if (this.config.cron) {
                this._scheduleObj = schedule.scheduleJob(this.config.cron, this.execute.bind(this));
            }
            else if (this.config.interval) {
                let intervalTime = Index_1.TimeUtils.timeStrToNumber(this.config.interval);
                if (intervalTime > 0) {
                    this._timer = timers_1.setInterval(this.execute.bind(this), intervalTime * 1000);
                }
            }
            if (this.config.justRun) {
                yield this.execute();
            }
        });
    }
    execute() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.config.master && !global.config.bMaster)
                return;
            try {
                yield this.mExecute();
                this.log.debug("计划任务执行[" + this.constructor.name + "]");
            }
            catch (e) {
                this.log.error("计划任务出错[" + this.constructor.name + "]\n" + e.message);
            }
        });
    }
    stop() {
        if (!this._isStart)
            return;
        this._isStart = false;
        if (this._timer) {
            timers_1.clearInterval(this._timer);
            this._timer = null;
        }
        if (this._scheduleObj) {
            this._scheduleObj.cancel();
            this._scheduleObj = null;
        }
    }
    get log() {
        return global.log;
    }
}
exports.ScheduleBase = ScheduleBase;
//# sourceMappingURL=ScheduleBase.js.map