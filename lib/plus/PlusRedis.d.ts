import { PlusBase } from "./PlusBase";
import { Redis } from "ioredis";
import { IRedisOption } from "../entity/IConfig";
declare var Redis: any;
export declare class PlusRedis extends PlusBase {
    static NAME: string;
    private _options;
    private _redis;
    constructor(options: IRedisOption);
    init(): Promise<void>;
    private _onReady;
    private _onError;
    get client(): Redis;
}
export {};
