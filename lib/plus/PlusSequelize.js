"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PlusSequelize = void 0;
const ClsUtils_1 = require("../utils/ClsUtils");
const FileUtils_1 = require("../utils/FileUtils");
const PlusBase_1 = require("./PlusBase");
const ModelBase_1 = require("./sequelize/ModelBase");
const ModelConfigBase_1 = require("./sequelize/ModelConfigBase");
const sequelize = require('sequelize');
class PlusSequelize extends PlusBase_1.PlusBase {
    constructor(config, modelPath, options, name) {
        super(name || PlusSequelize.NAME);
        var defaultOpt = {
            host: "127.0.0.1",
            dialect: "mysql",
            port: 3306,
            pool: {
                max: 300,
                min: 1,
                idle: 20000,
                acquire: 20000,
                evict: 30000,
                handleDisconnects: true,
                connectRetries: 3,
                acquireTimeoutMillis: 0
            },
            timezone: "+08:00",
            benchmark: true,
            logging: this._onLog.bind(this)
        };
        this._option = Object.assign({}, defaultOpt, config.options, options);
        this.mModelPath = modelPath;
        this._sequelize = new sequelize(config.dbName, config.user, config.pwd, this._option);
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            this.mModelHash = {};
            yield this.mInitModelPath();
            this.mCompleteHandle();
        });
    }
    mInitModelPath() {
        return __awaiter(this, void 0, void 0, function* () {
            let jsFiles = FileUtils_1.FileUtils.readFile(this.mModelPath, "js");
            for (let i = 0; i < jsFiles.length; i++) {
                let fileName = jsFiles[i];
                var d = require(this.mModelPath + "//" + fileName);
                if (d) {
                    let configCls;
                    let modelCls;
                    for (var key in d) {
                        if (ClsUtils_1.ClsUtils.targetIsCls(d[key], ModelConfigBase_1.ModelConfigBase)) {
                            configCls = d[key];
                        }
                        else if (ClsUtils_1.ClsUtils.targetIsCls(d[key], ModelBase_1.ModelBase)) {
                            modelCls = d[key];
                        }
                    }
                    if (configCls && modelCls) {
                        let config = new configCls(configCls.TABLE_NAME);
                        if (config.name) {
                            yield this.initModel(config, modelCls);
                        }
                    }
                }
            }
        });
    }
    initModel(config, modelCls) {
        return __awaiter(this, void 0, void 0, function* () {
            config.init();
            let options = config.options;
            options.sequelize = this._sequelize;
            modelCls.init(config.attr, options);
            this.mModelHash[modelCls.tableName] = modelCls;
            if (config.needSync) {
                yield modelCls.sync({ alter: config.alter });
            }
        });
    }
    defineModel(config) {
        config.init();
        let model = this._sequelize.define(config.name, config.attr, config.options);
        return model;
    }
    getModel(name) {
        return this._sequelize.model(name);
    }
    mGetValues() {
        var arr = this["_options"].attributes;
        var obj = {};
        if (arr) {
            for (var i = 0; i < arr.length; i++) {
                obj[arr[i]] = this[arr[i]];
            }
        }
        else {
            for (var key in this["dataValues"]) {
                obj[key] = this[key];
            }
        }
        return obj;
    }
    _onLog(sql, time) {
        this.emit("record", { sql_text: sql.replace('Executed (default):', ''), cost_time: time });
    }
}
exports.PlusSequelize = PlusSequelize;
PlusSequelize.NAME = "sequelize";
//# sourceMappingURL=PlusSequelize.js.map