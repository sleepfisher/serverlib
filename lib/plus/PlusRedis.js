"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PlusRedis = void 0;
const PlusBase_1 = require("./PlusBase");
const ioredis_1 = require("ioredis");
var Redis = require("ioredis");
class PlusRedis extends PlusBase_1.PlusBase {
    constructor(options) {
        super(PlusRedis.NAME);
        this._options = options;
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this._options.cluster) {
                let nodes = this._options.cluster;
                delete this._options.cluster;
                this._redis = new ioredis_1.Cluster(nodes, { redisOptions: this._options });
            }
            else {
                this._redis = new Redis(this._options);
            }
            this._redis.on("ready", this._onReady.bind(this));
            this._redis.on("disconnect", () => {
                this._redis.connect(() => {
                    this.log.info("reconnect success");
                });
            });
            this._redis.on('reconnecting', (reply) => {
                this.log.warn('RedisReconnecting--' + reply);
            });
            this._redis.on('error', this._onError.bind(this));
        });
    }
    _onReady() {
        this.mCompleteHandle();
    }
    _onError(err) {
        this.log.error('RedisErr--' + err);
        this.mOnFail(err);
    }
    get client() {
        return this._redis;
    }
}
exports.PlusRedis = PlusRedis;
PlusRedis.NAME = "redis";
//# sourceMappingURL=PlusRedis.js.map