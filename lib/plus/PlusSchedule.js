"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PlusSchedule = void 0;
const PlusBase_1 = require("./PlusBase");
const path_1 = require("path");
const ScheduleBase_1 = require("./schedule/ScheduleBase");
const ClsUtils_1 = require("../utils/ClsUtils");
const FileUtils_1 = require("../utils/FileUtils");
class PlusSchedule extends PlusBase_1.PlusBase {
    constructor(path) {
        super(PlusSchedule.NAME);
        this._path = path;
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            let jsFiles = FileUtils_1.FileUtils.readFile(this._path, "js");
            for (let i = 0; i < jsFiles.length; i++) {
                let d = require(path_1.join(this._path, jsFiles[i]));
                for (let key in d) {
                    if (ClsUtils_1.ClsUtils.targetIsCls(d[key], ScheduleBase_1.ScheduleBase)) {
                        yield this.regisSchedule(new d[key]());
                    }
                }
            }
            this.mCompleteHandle();
        });
    }
    regisSchedule(scheduleItem) {
        return __awaiter(this, void 0, void 0, function* () {
            yield scheduleItem.start();
        });
    }
}
exports.PlusSchedule = PlusSchedule;
PlusSchedule.NAME = "schedule";
//# sourceMappingURL=PlusSchedule.js.map