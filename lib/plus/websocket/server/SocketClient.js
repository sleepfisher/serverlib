"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketClient = void 0;
const events_1 = require("events");
const HeartTime = 20000;
class SocketClient extends events_1.EventEmitter {
    constructor(ws, key, ip, port) {
        super();
        this._id = '';
        this._key = '';
        this._updateTime = 0;
        this._timeId = 0;
        this._key = key;
        this._id = "L" + (++SocketClient.Link_Count);
        this.setTo(ws, ip, port);
    }
    setTo(ws, ip, port) {
        if (this._ws) {
            this._ws.close();
            this._ws.removeAllListeners();
        }
        this._ip = ip;
        this._port = port;
        this._ws = ws;
        var _self = this;
        ws.on('message', (e) => {
            _self._onMessage(e);
        });
        ws.on('close', (e) => {
            _self._onClose(e);
        });
        this._updateTime = Date.now();
        if (!this._timeId) {
            this._timeId = setInterval(this.checkHeart.bind(this), 10000);
        }
    }
    get id() {
        return this._id;
    }
    get ip() {
        return this._ip;
    }
    get port() {
        return this._port;
    }
    get key() {
        return this._key;
    }
    send(d) {
        if (!d)
            return;
        var msg = d;
        try {
            this._ws.send(msg);
        }
        catch (e) {
            console.error(e);
        }
    }
    checkHeart() {
        if (Date.now() - this._updateTime > HeartTime) {
            this._close();
        }
    }
    _stopHeart() {
        if (this._timeId) {
            clearInterval(this._timeId);
        }
        this._timeId = null;
    }
    close() {
        this._close();
    }
    _onMessage(d) {
        this._updateTime = Date.now();
        this.emit("message", this._id, d, this);
    }
    _onClose(e) {
        this._ws = null;
        this._close();
    }
    _close() {
        this._stopHeart();
        if (this._ws) {
            this._ws.close(4001, "tick");
            this._ws.removeAllListeners();
        }
        this._ws = null;
        this.emit("close", this._id);
    }
}
exports.SocketClient = SocketClient;
SocketClient.Link_Count = 0;
//# sourceMappingURL=SocketClient.js.map