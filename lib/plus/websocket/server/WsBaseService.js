"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RegistMethodVo = exports.WsBaseService = void 0;
const BaseEntity_1 = require("../../../entity/BaseEntity");
const Code_1 = require("../../../entity/Code");
const PlusRedis_1 = require("../../PlusRedis");
class WsBaseService {
    constructor(linkId, parent) {
        this._methods = {};
        this._linkId = linkId;
        this._parent = parent;
        this.mInitFuns();
    }
    _regist(func) {
        var str = func.toString().split("\r\n")[0];
        var arr;
        if (str.indexOf("(") != -1 && str.indexOf(")") != -1) {
            str = str.split("(")[1].split(")")[0];
            str = str.replace(/\s+/g, "");
            arr = str.split(",");
            for (var i = 0; i < arr.length; i++) {
                var dengIndex = arr[i].indexOf("=");
                if (dengIndex != -1) {
                    arr[i] = String(arr[i]).substring(0, dengIndex);
                }
            }
        }
        let methodVo = new RegistMethodVo(this, func, func.name, ...arr);
        this._methods[methodVo.name] = methodVo;
    }
    execute(funcName, cmd, data, id, client) {
        return __awaiter(this, void 0, void 0, function* () {
            let methodVo = this._methods[funcName];
            this._client = client;
            if (methodVo) {
                if (!this.mAuthentication(funcName, data, client))
                    return;
                let startT = Date.now();
                var arg = [];
                for (var i = 0; data && methodVo.args && i < methodVo.args.length; i++) {
                    arg.push(data[methodVo.args[i]]);
                }
                let response;
                try {
                    let result = yield methodVo.func.call(methodVo.caller, ...arg);
                    if (result) {
                        response = new BaseEntity_1.BaseResp(0, result);
                    }
                }
                catch (err) {
                    global.log.error("失败" + cmd + "[" + client.ip + "]----" + (data ? JSON.stringify(data) : "") + "\r\n" + err.stack);
                    response = new BaseEntity_1.BaseResp(err.code || Code_1.EnumCode.FAIL, {}, global.Debug ? err.stack : err.message);
                }
                response && this._parent.sendCmd(this._linkId, cmd, response, id);
                global.log.trace(`执行${cmd}消耗${Date.now() - startT}ms`);
            }
        });
    }
    throwError(message, code) {
        throw new BaseEntity_1.BaseError(message, code);
    }
    mAuthentication(funcName, data, client) {
        return true;
    }
    get log() {
        return global.log;
    }
    get redis() {
        return global.plus.getPlus(PlusRedis_1.PlusRedis).client;
    }
}
exports.WsBaseService = WsBaseService;
class RegistMethodVo {
    constructor(caller, func, name, ...args) {
        this.func = func;
        this.name = name;
        this.args = args;
        this.caller = caller;
    }
}
exports.RegistMethodVo = RegistMethodVo;
//# sourceMappingURL=WsBaseService.js.map