"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WsServer = void 0;
const events_1 = require("events");
const path_1 = require("path");
const ws_1 = require("ws");
const Index_1 = require("../../../Index");
const HttpUtils_1 = require("../../../utils/HttpUtils");
const JsonProtocol_1 = require("../protocol/JsonProtocol");
const SocketClient_1 = require("./SocketClient");
const WsBaseService_1 = require("./WsBaseService");
var crypto = require('crypto');
function createkey() {
    var md5 = crypto.createHash('md5');
    var d = { time: Date.now(), key: Math.random() };
    return md5.update(JSON.stringify(d)).digest('hex');
}
class WsServer extends events_1.EventEmitter {
    constructor(port, path, protocol, isProxy = false) {
        super();
        this._linkCount = 0;
        this._port = port;
        this._path = path;
        this._isProxy = isProxy;
        this._serverHash = {};
        this._protocol = protocol || WsServer.DefaultProtocol;
    }
    init() {
        this._ws = new ws_1.Server({
            port: this._port,
            verifyClient: this._socketVerify.bind(this)
        });
        this._clientHash = {};
        this._keyHash = {};
        this._ws.on('connection', this._onConnect.bind(this));
        let jsFiles = Index_1.FileUtils.readFile(this._path, "js");
        for (var f of jsFiles) {
            let mapping = require(path_1.join(this._path, f));
            for (var key in mapping) {
                if (Index_1.ClsUtils.targetIsCls(mapping[key], WsBaseService_1.WsBaseService)) {
                    this._serverHash[key] = mapping[key];
                }
            }
        }
    }
    _socketVerify(info) {
        if (this.verifyFunc && !this.verifyFunc(info))
            return false;
        var key = info.req.headers["sec-websocket-protocol"];
        if (!key) {
            key = createkey();
        }
        else if (!this.getClientByKey(key)) {
            return false;
        }
        info.req.socket.key = key;
        return true;
    }
    getClient(link) {
        return this._clientHash[link];
    }
    getClientByKey(key) {
        if (this._keyHash[key] && this._clientHash[this._keyHash[key]]) {
            return this._clientHash[this._keyHash[key]];
        }
        return null;
    }
    closeClient(linkId) {
        var client = this.getClient(linkId);
        if (client) {
            client.close();
        }
    }
    send(linkId, data) {
        var client = this.getClient(linkId);
        if (client) {
            client.send(data);
        }
    }
    sendCmd(linkId, cmd, data, id = 0) {
        var client = this.getClient(linkId);
        if (client) {
            var msg = this._protocol.encode(cmd, data, id);
            client.send(msg);
        }
    }
    _onConnect(ws, req) {
        var socket = ws._socket;
        var key = socket["key"];
        var socketClient = this.getClientByKey(key);
        var ip = HttpUtils_1.HttpUtils.getIp(req, this._isProxy);
        var port = socket.remotePort;
        if (socketClient) {
            socketClient.setTo(ws, ip, port);
            console.log(socketClient.id + "重连了");
            return;
        }
        socketClient = new SocketClient_1.SocketClient(ws, key, ip, port);
        global.log.debug("新登录id:" + socketClient.id + "-----" + key + "---" + ip + ":" + port);
        this._clientHash[socketClient.id] = socketClient;
        this._keyHash[key] = socketClient.id;
        socketClient.on('message', this._onMessage.bind(this));
        socketClient.on('close', this._onClose.bind(this));
        this.emit("connect", socketClient);
    }
    _onMessage(link, msg, client) {
        return __awaiter(this, void 0, void 0, function* () {
            var msgData;
            try {
                msgData = this._protocol.decode(msg);
            }
            catch (e) {
                global.log.error(client.id + ":" + msg);
                return;
            }
            if (msgData == null)
                return;
            var cmd = msgData.cmd;
            var id = msgData.id;
            var data = msgData.data;
            if (cmd) {
                var cmdArr = cmd.split(".");
                var serverName = cmdArr[0];
                var func = cmdArr[1];
                var serverCls = this._serverHash[serverName];
                if (serverCls && serverCls.prototype[func]) {
                    var serverCmd = new serverCls(link, this);
                    yield serverCmd.execute(func, cmd, data, id, client);
                }
            }
            this.emit("message", link, msgData);
        });
    }
    _onClose(link) {
        delete this._clientHash[link];
        for (var key in this._keyHash) {
            var id = this._keyHash[key];
            if (id == link) {
                delete this._keyHash[key];
                break;
            }
        }
        this.emit("closeClient", link);
        global.log.debug("断开了--" + link);
    }
}
exports.WsServer = WsServer;
WsServer.NAME = "PlusWsServer";
WsServer.DefaultProtocol = new JsonProtocol_1.JsonProtocol();
//# sourceMappingURL=WsServer.js.map