/// <reference types="node" />
import { EventEmitter } from "events";
export declare class SocketClient extends EventEmitter {
    static Link_Count: number;
    private _id;
    private _key;
    private _ws;
    private _updateTime;
    private _timeId;
    private _ip;
    private _port;
    constructor(ws: any, key: any, ip: string, port: number);
    setTo(ws: any, ip: string, port: number): void;
    get id(): string;
    get ip(): string;
    get port(): number;
    get key(): string;
    send(d: Buffer | string): void;
    checkHeart(): void;
    private _stopHeart;
    close(): void;
    private _onMessage;
    private _onClose;
    private _close;
}
