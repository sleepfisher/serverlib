/// <reference types="ioredis" />
import { SocketClient } from "./SocketClient";
import { WsServer } from "./WsServer";
export declare abstract class WsBaseService {
    private _methods;
    protected _linkId: string;
    protected _client: SocketClient;
    private _parent;
    constructor(linkId: string, parent: WsServer);
    protected abstract mInitFuns(): any;
    protected _regist(func: Function): void;
    execute(funcName: string, cmd: string, data: any, id: number, client: SocketClient): Promise<void>;
    throwError(message: string, code?: number): void;
    protected mAuthentication(funcName: string, data: any, client: SocketClient): boolean;
    get log(): import("../../../Index").LogUtils;
    get redis(): import("ioredis").Redis;
}
export declare class RegistMethodVo {
    func: Function;
    name: string;
    args: Array<any>;
    caller: any;
    uri: string;
    constructor(caller: any, func: Function, name: string, ...args: any[]);
}
