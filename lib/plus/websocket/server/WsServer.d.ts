/// <reference types="node" />
import { EventEmitter } from "events";
import { IProtocol } from "../protocol/IProtocol";
import { SocketClient } from "./SocketClient";
export declare class WsServer extends EventEmitter {
    static NAME: string;
    static DefaultProtocol: IProtocol;
    private _ws;
    private _clientHash;
    private _keyHash;
    private _linkCount;
    private _port;
    private _path;
    private _serverHash;
    private _protocol;
    protected _isProxy: boolean;
    verifyFunc: (info: object) => boolean;
    constructor(port: number, path: string, protocol?: IProtocol, isProxy?: boolean);
    init(): void;
    protected _socketVerify(info: any): boolean;
    getClient(link: string): SocketClient;
    getClientByKey(key: string): SocketClient;
    closeClient(linkId: string): void;
    send(linkId: string, data: any): void;
    sendCmd(linkId: string, cmd: string, data: any, id?: number): void;
    private _onConnect;
    protected _onMessage(link: string, msg: any, client: SocketClient): Promise<void>;
    protected _onClose(link: any): void;
}
