"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JsonProtocol = void 0;
class JsonProtocol {
    decode(buff) {
        if (buff == this.heartData)
            return null;
        var data = JSON.parse(buff);
        return data;
    }
    encode(cmd, data, id) {
        return JSON.stringify({ cmd: cmd, data: data, id: id });
    }
    get heartData() { return "heart"; }
}
exports.JsonProtocol = JsonProtocol;
//# sourceMappingURL=JsonProtocol.js.map