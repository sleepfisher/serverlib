"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProtobufProtocol = void 0;
const FileUtils_1 = require("../../../utils/FileUtils");
const protobufjs_1 = require("protobufjs");
const path_1 = require("path");
const _ = require("lodash");
class ProtobufProtocol {
    constructor() {
        this._proto = null;
    }
    loadProtoDir(dirPath) {
        let protoFiles = FileUtils_1.FileUtils.readFile(dirPath, "proto");
        protoFiles = protoFiles.map(fileName => path_1.join(dirPath, fileName));
        this._proto = protobufjs_1.loadSync(protoFiles).nested;
    }
    decode(buff) {
        if (buff.length == 5 && buff.toString() == this.heartData)
            return null;
        var msgSize = buff.readUInt32LE(0);
        var id = buff.readUInt32LE(4);
        var code = buff.readUInt8(8);
        var cmdSize = buff.readUInt16LE(9);
        var cmd = buff.toString("utf8", 11, 11 + cmdSize);
        let model = this.lookup(cmd + "Request");
        if (!model) {
            throw new TypeError(`${cmd} not found, please check it again`);
        }
        var data = model.decode(buff.slice(11 + cmdSize));
        return { cmd: cmd, id: id, data: data };
    }
    encode(cmd, data, id) {
        let model = this.lookup(cmd + "Response");
        if (!model) {
            throw new TypeError(`${cmd} not found, please check it again`);
        }
        var buffer = model.encode(data.data).finish();
        var head = Buffer.alloc(11 + cmd.length);
        head.writeUInt32LE(id, 4);
        head.writeUInt8(data.code, 8);
        head.writeUInt16LE(cmd.length, 9);
        head.write(cmd, 11, "utf8");
        head.writeUInt32LE(head.length + buffer.length - 4, 0);
        return Buffer.concat([head, buffer]);
    }
    get heartData() { return "heart"; }
    lookup(protoName) {
        if (!this._proto) {
            throw new TypeError('Please load proto before lookup');
        }
        return _.get(this._proto, protoName);
    }
}
exports.ProtobufProtocol = ProtobufProtocol;
//# sourceMappingURL=ProtobufProtocol.js.map