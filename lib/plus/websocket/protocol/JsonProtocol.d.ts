/// <reference types="node" />
import { IProtocol } from "./IProtocol";
import { IProtocolData } from "./IProtocolData";
export declare class JsonProtocol implements IProtocol {
    decode(buff: string): IProtocolData;
    encode(cmd: string, data: any, id?: number): Buffer | string;
    get heartData(): string;
}
