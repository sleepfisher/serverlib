/// <reference types="node" />
import { IProtocol } from "./IProtocol";
import { IProtocolData } from "./IProtocolData";
import { BaseResp } from "../../../entity/BaseEntity";
export declare class ProtobufProtocol implements IProtocol {
    private _proto;
    loadProtoDir(dirPath: string): void;
    decode(buff: Buffer): IProtocolData;
    encode(cmd: string, data: BaseResp, id?: number): Buffer | string;
    get heartData(): string;
    private lookup;
}
