/// <reference types="node" />
import { IProtocolData } from "./IProtocolData";
export interface IProtocol {
    decode(buff: Buffer | string): IProtocolData;
    encode?(cmd: string, data: any, id?: number): Buffer | string;
    encode?(...args: any[]): Buffer | string;
}
