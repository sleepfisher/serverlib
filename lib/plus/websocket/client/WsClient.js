"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WsClient = void 0;
const events_1 = require("events");
const WsServer_1 = require("../server/WsServer");
const WebSocket = require('ws');
class WsClient extends events_1.EventEmitter {
    constructor(opt) {
        super();
        this._protocol = opt && opt.protocol || WsServer_1.WsServer.DefaultProtocol;
        this._autoReconnect = opt && opt.autoReconnect || true;
        this._reconnectCount = opt && opt.reconnectCount || Number.MAX_SAFE_INTEGER;
        this._reconnectDelay = opt && opt.reconnectDelay || 10;
        this._connectCount = 0;
    }
    connect(host, protocol) {
        this.close();
        this._host = host;
        this._idIndex = 0;
        this._ws = new WebSocket(host, protocol);
        var self = this;
        this._ws.on('message', (e) => {
            self._onMessage(e);
        });
        this._ws.on('close', (e) => {
            self._onClose(e);
        });
        this._ws.on('error', (e) => {
            self._onError(e);
        });
        this._ws.on("open", (e) => {
            self._onOpen(e);
        });
    }
    get readyState() {
        return this._ws ? this._ws.readyState : WebSocket.CLOSED;
    }
    send(cmd, data, callback) {
        let id = ++this._idIndex;
        var buffer = this._protocol.encode(cmd, data, id);
        if (callback != null) {
            this._callbackHandle[id] = callback;
        }
        this._ws.send(buffer);
    }
    sendAsync(cmd, data) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                this.send(cmd, data, (response) => {
                    resolve(response);
                });
            });
        });
    }
    close() {
        this._clear();
    }
    _onOpen(e) {
        this.emit("connect", e);
        this._connectCount = 0;
        this._startHeart();
    }
    _onMessage(msg) {
        var msgData;
        try {
            msgData = this._protocol.decode(msg);
        }
        catch (e) {
            global.log.error(this._host + ":" + msg);
            return;
        }
        if (msgData == null)
            return;
        let id = msgData.id;
        let callback = this._callbackHandle[id];
        if (callback != null)
            callback(msgData.data);
        this.emit("message", msgData);
    }
    _onClose(e) {
        this._stopHeart();
        if (this._ws != null && this._autoReconnect && this._connectCount < this._reconnectCount) {
            this._reconnect();
        }
        else {
            this.emit("close", e);
        }
    }
    _onError(e) {
        this.emit("error", e);
    }
    _startHeart() {
        this._stopHeart();
        this._heartTime = setInterval(this._sendHeart.bind(this), 10000);
    }
    _stopHeart() {
        if (this._heartTime) {
            clearInterval(this._heartTime);
            this._heartTime = null;
        }
    }
    _sendHeart() {
        this._ws.send("heart");
    }
    _reconnect() {
        this.emit("reconnect");
        this._connectCount++;
        let delayTime = this._reconnectDelay * (Math.ceil(this._connectCount / 5)) * 1000;
        this._reconnectTimer = setTimeout(() => {
            this._reconnectTimer = null;
            this.connect(this._host);
        }, delayTime);
    }
    _clear() {
        if (this._ws != null) {
            if (this._ws.CONNECTING || this._ws.OPEN) {
                this._ws.close();
            }
            this._ws = null;
        }
        if (this._reconnectTimer != null) {
            clearTimeout(this._reconnectTimer);
            this._reconnectTimer = null;
        }
        this._callbackHandle = {};
    }
}
exports.WsClient = WsClient;
//# sourceMappingURL=WsClient.js.map