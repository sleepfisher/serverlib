/// <reference types="node" />
import { EventEmitter } from "events";
import { BaseResp } from "../../../entity/BaseEntity";
import { IProtocol } from "../protocol/IProtocol";
declare type SocketMessageBackHandle = (data: BaseResp) => void;
export interface IWsClientOptions {
    protocol?: IProtocol;
    autoReconnect?: boolean;
    reconnectCount?: number;
    reconnectDelay?: number;
}
export declare class WsClient extends EventEmitter {
    private _ws;
    private _host;
    private _protocol;
    private _idIndex;
    private _autoReconnect;
    private _reconnectCount;
    private _reconnectDelay;
    private _connectCount;
    private _callbackHandle;
    private _reconnectTimer;
    constructor(opt?: IWsClientOptions);
    connect(host: string, protocol?: string): void;
    get readyState(): any;
    send(cmd: string, data: any, callback?: SocketMessageBackHandle): void;
    sendAsync(cmd: string, data: any): Promise<BaseResp>;
    close(): void;
    private _onOpen;
    private _onMessage;
    private _onClose;
    private _onError;
    private _heartTime;
    private _startHeart;
    private _stopHeart;
    private _sendHeart;
    private _reconnect;
    private _clear;
}
export {};
