"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileUtils = void 0;
const fs_1 = require("fs");
class FileUtils {
    static readFile(path, fileType) {
        if (!fs_1.existsSync(path))
            return [];
        let files = fs_1.readdirSync(path);
        files = files.filter((f) => {
            return f.endsWith("." + fileType);
        });
        return files;
    }
}
exports.FileUtils = FileUtils;
//# sourceMappingURL=FileUtils.js.map