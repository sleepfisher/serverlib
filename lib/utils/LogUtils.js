"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LogUtils = void 0;
var log4js = require('log4js');
const outLogPath = "${workspace}/logs/out/out";
const excepitonLogPath = "${workspace}/logs/error/err";
var logConfigs = {
    appenders: {
        console: {
            type: 'console',
        },
        access: {
            type: 'dateFile',
            filename: outLogPath,
            pattern: 'yyyy-MM-dd-hh.log',
            alwaysIncludePattern: true,
            daysToKeep: 7
        },
        errorFile: {
            type: 'dateFile',
            filename: excepitonLogPath,
            pattern: 'yyyy-MM-dd.log',
            alwaysIncludePattern: true,
            daysToKeep: 30
        },
    },
    categories: {
        default: { appenders: ['access', 'console'], level: "INFO" },
        errorFile: { appenders: ['errorFile', 'console'], level: "ERROR" }
    }
};
class LogUtils {
    constructor(workspace, logPath, configs) {
        if (configs) {
            if (configs.appenders) {
                for (let key in configs.appenders) {
                    logConfigs.appenders[key] = configs.appenders[key];
                }
            }
            if (configs.categories) {
                for (let key in configs.categories) {
                    logConfigs.categories[key] = configs.categories[key];
                }
            }
        }
        for (let key in logConfigs.appenders) {
            let appender = logConfigs.appenders[key];
            if (appender.filename) {
                appender.filename = appender.filename.replace(/\$\{workspace\}/g, workspace).replace(/\$\{logspace\}/g, logPath);
            }
        }
        log4js.configure(logConfigs);
        this._logs = {};
        for (let key in logConfigs.categories) {
            this._logs[key] = log4js.getLogger(key);
        }
        this._info = this._logs["default"];
        this._errLog = this._logs["errorFile"];
    }
    updateLogLevel(logLevels) {
        if (!logLevels)
            return;
        for (let key in logLevels) {
            let logger = this._logs[key];
            if (logger) {
                logger.level = logLevels[key];
            }
        }
    }
    trace(message, ...args) {
        this._info.trace(message, ...args);
    }
    debug(message, ...args) {
        this._info.debug(message, ...args);
    }
    info(message, ...args) {
        this._info.info(message, ...args);
    }
    warn(message, ...args) {
        this._info.warn(message, ...args);
    }
    error(message, ...args) {
        this._errLog.error(message, ...args);
    }
    fatal(message, ...args) {
        this._info.fatal(message, ...args);
    }
    getLogger(type) {
        return this._logs[type];
    }
}
exports.LogUtils = LogUtils;
//# sourceMappingURL=LogUtils.js.map