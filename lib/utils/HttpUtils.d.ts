export declare class HttpUtils {
    static get(url: string, data?: any, responseType?: string): Promise<any>;
    static post(url: string, data?: any, responseType?: string, headers?: any): Promise<any>;
    static getIp(requestMsg: any, isProxy: boolean): string;
    static getLocalIp(): any;
    private static _getEncoding;
    static logError(url: string, data: any, method: string, error: any, response?: any): void;
}
