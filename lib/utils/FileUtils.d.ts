export declare class FileUtils {
    static readFile(path: string, fileType: string): string[];
}
