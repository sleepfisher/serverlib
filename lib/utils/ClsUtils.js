"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClsUtils = void 0;
class ClsUtils {
    static targetIsCls(target, cls) {
        if (!Object.getPrototypeOf(target)) {
            return false;
        }
        if (Object.getPrototypeOf(target).toString() !== cls.toString()) {
            return this.targetIsCls(Object.getPrototypeOf(target), cls);
        }
        return true;
    }
}
exports.ClsUtils = ClsUtils;
//# sourceMappingURL=ClsUtils.js.map