export declare class TimeUtils {
    static timeStrToNumber(str: string): number;
    static getMinuteString(timeStamp: number): string;
}
