"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StringUtils = void 0;
const uuidv1 = require("uuid/v1");
const moment = require("moment");
const md5Hex = require('md5-hex');
class StringUtils {
    static getGeneralId(key) {
        return md5Hex(key + moment().format('YYYYMMDDHmmss') + '_' + uuidv1());
    }
    static isValidStringByLength(str, mix, max) {
        if (!str || typeof (str) != 'string')
            return false;
        if (str.length < mix || (max > 0 && str.length > max))
            return false;
        return true;
    }
    static getSortKeys(obj, excludeNull = false, encode = false) {
        var keyList = [];
        for (var key in obj) {
            if (excludeNull && (obj[key] === null && obj[key] === ""))
                continue;
            keyList.push(key);
        }
        keyList.sort();
        var stringA = "";
        for (var i = 0; i < keyList.length - 1; i++) {
            let value = obj[keyList[i]];
            if (encode)
                value = encodeURIComponent(value);
            stringA += keyList[i] + "=" + value + "&";
        }
        let value = obj[keyList[i]];
        if (encode)
            value = encodeURIComponent(value);
        stringA += keyList[i] + "=" + value;
        return stringA;
    }
}
exports.StringUtils = StringUtils;
//# sourceMappingURL=StringUtils.js.map