"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpUtils = void 0;
const bent = require('bent');
const qs = require("querystring");
class HttpUtils {
    static get(url, data, responseType = "json") {
        return __awaiter(this, void 0, void 0, function* () {
            if (data) {
                if (url.indexOf("?") == -1) {
                    url += "?";
                }
                else {
                    url += "&";
                }
                url += qs.stringify(data);
            }
            let requestFunc = bent("GET", this._getEncoding(responseType));
            try {
                let result = yield requestFunc(url);
                return result;
            }
            catch (error) {
                this.logError(url, data, "get", error);
                throw error;
            }
        });
    }
    static post(url, data, responseType = "json", headers) {
        return __awaiter(this, void 0, void 0, function* () {
            let header = { 'content-type': 'application/json' };
            if (headers) {
                Object.assign(header, headers);
            }
            let requestFunc = bent("POST", this._getEncoding(responseType));
            try {
                let result = yield requestFunc(url, data, header);
                return result;
            }
            catch (error) {
                this.logError(url, data, "post", error);
                throw error;
            }
        });
    }
    static getIp(requestMsg, isProxy) {
        var ip = isProxy ? requestMsg.headers["x-forwarded-for"] : requestMsg.connection.remoteAddress;
        ip = ip.replace("::ffff:", "");
        return ip;
    }
    static getLocalIp() {
        const os = require('os');
        var interfaces = os.networkInterfaces();
        for (var devName in interfaces) {
            var iface = interfaces[devName];
            for (var i = 0; i < iface.length; i++) {
                var alias = iface[i];
                if (alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal) {
                    return alias.address;
                }
            }
        }
    }
    static _getEncoding(responseType) {
        if (responseType == "text" || responseType == "string") {
            return "string";
        }
        else if (responseType == "buffer") {
            return "buffer";
        }
        else {
            return "json";
        }
    }
    static logError(url, data, method, error, response) {
        if (global.log) {
            if (response) {
                global.log.error("请求拒绝:\nurl=[" + url + "]\ndata=[" + JSON.stringify(data || {}) + "]\nmethod=[" + method + "]\nerror=[" + JSON.stringify(error || {}) + "]\nresponse=[" + JSON.stringify(response || {}) + "]");
            }
            else {
                global.log.error("请求出错:\nurl=[" + url + "]\ndata=[" + JSON.stringify(data || {}) + "]\nmethod=[" + method + "]\nerror=[" + JSON.stringify(error) + "]");
            }
        }
    }
}
exports.HttpUtils = HttpUtils;
//# sourceMappingURL=HttpUtils.js.map