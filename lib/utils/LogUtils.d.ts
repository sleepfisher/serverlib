import { Configuration, Logger } from "log4js";
import { EnumLogLevel } from "../entity/IConfig";
export declare class LogUtils {
    private _info;
    private _errLog;
    private _logs;
    constructor(workspace: string, logPath: string, configs?: Configuration);
    updateLogLevel(logLevels: {
        [key: string]: EnumLogLevel;
    }): void;
    trace(message: any, ...args: any[]): void;
    debug(message: any, ...args: any[]): void;
    info(message: any, ...args: any[]): void;
    warn(message: any, ...args: any[]): void;
    error(message: any, ...args: any[]): void;
    fatal(message: any, ...args: any[]): void;
    getLogger(type: string): Logger;
}
