"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimeUtils = void 0;
class TimeUtils {
    static timeStrToNumber(str) {
        if (str == "-1")
            return -1;
        if (typeof str != "string")
            return str;
        var timeCode = str.charAt(str.length - 1);
        if (timeCode == "s")
            return parseInt(str.substr(0, str.length - 1));
        if (timeCode == "m")
            return parseInt(str.substr(0, str.length - 1)) * 60;
        if (timeCode == "h")
            return parseInt(str.substr(0, str.length - 1)) * 3600;
        if (timeCode == "d")
            return parseInt(str.substr(0, str.length - 1)) * 86400;
        return parseInt(str.substr(0, str.length - 1));
    }
    static getMinuteString(timeStamp) {
        const date = new Date(timeStamp);
        let y = date.getFullYear();
        let m = date.getMonth() + 1;
        let MM = m < 10 ? ('0' + m) : m;
        let d = date.getDate();
        let DD = d < 10 ? ('0' + d) : d;
        let h = date.getHours();
        let HH = h < 10 ? ('0' + h) : h;
        let minute = date.getMinutes();
        let min = minute < 10 ? ('0' + minute) : minute;
        let second = date.getSeconds();
        let sec = second < 10 ? ('0' + second) : second;
        return y + '-' + MM + '-' + DD + ' ' + HH + ':' + min + ':' + sec;
    }
}
exports.TimeUtils = TimeUtils;
//# sourceMappingURL=TimeUtils.js.map