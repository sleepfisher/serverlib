export declare class StringUtils {
    static getGeneralId(key: string): any;
    static isValidStringByLength(str: string, mix: number, max: number): boolean;
    static getSortKeys(obj: any, excludeNull?: boolean, encode?: boolean): string;
}
