import { BaseError } from "./entity/BaseEntity";
import { EnumCode } from "./entity/Code";
import { processArg } from "./entity/Config";
import { IGlobal } from "./entity/Global";
import { BaseService } from "./plus/httpServer/BaseService";
import { PlusHttpServer } from "./plus/PlusHttpServer";
import { PlusRedis } from "./plus/PlusRedis";
import { PlusSchedule } from "./plus/PlusSchedule";
import { PlusSequelize } from "./plus/PlusSequelize";
import { PlusWsServer } from "./plus/PlusWsServer";
import { WsBaseService } from "./plus/websocket/server/WsBaseService";
import { PlusMgr } from "./PlusMgr";
import { ClsUtils } from "./utils/ClsUtils";
import { FileUtils } from "./utils/FileUtils";
import { HttpUtils } from "./utils/HttpUtils";
import { LogUtils } from "./utils/LogUtils";
import { StringUtils } from "./utils/StringUtils";
import { TimeUtils } from "./utils/TimeUtils";
export { PlusMgr, PlusHttpServer, PlusRedis, processArg, IGlobal, BaseService, EnumCode, PlusSchedule, PlusSequelize, BaseError, PlusWsServer, WsBaseService };
export { HttpUtils, StringUtils, FileUtils, TimeUtils, LogUtils, ClsUtils };
export declare class App {
    start(): void;
    protected mInitPlus(plusMgr: PlusMgr): void;
    protected mOnComplete(): Promise<void>;
    protected mConfigUpdate(): void;
    private handlerException;
}
