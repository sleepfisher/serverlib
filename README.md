# serverlib-fisher

#### 介绍
本库只是一个node服务端的基础库，支持插件方式使用功能。

### [更新列表](./CHANGELOG.md)

### 安装教程

```
npm i serverlib-fisher --save
```

### 基础插件：
    PlusHttpServer  基于koa的http服务插件
    PlusRedis       redis
    PlusSequelize   基于sequelize的orm数据库插件
    PlusSchedule    计划任务插件

启动服务 new App().start();

### 服务启动参数说明
启动命令node App.js --env=prod --port=1000
env代表环境，会根据不同的环境加载根目录下不同的config_${env}.json文件
port代表监听的服务端口
所有启动参数都在processArg对象上，结构参照--key=value

#### config结构说明
```
{
    "sql": {
        "options": {
            "host": "xxx.xxx.xxx.xxx",
            "port": "端口",
            "dialect": "mssql或者mysql"
        },
        "dbName": "db名",
        "user": "用户用",
        "pwd": "密码"
    },
    "redis": {
        "port": 6379,
        "host": "127.0.0.1",
        "db": 0
    },
    "port": 1000            //监听的端口,如果启动命令带了端口，则使用启动命令的
    "logLevels": {                      //通过修改default的日志等级，可以在服务开启的情况下调整日志级别。  日志服务使用的是log4js
        "default": "ALL"
    },
    "logConfig": {                  //增加自定义的日志口子
        "appenders": {
            "clientLog": {
                "type": "dateFile",
                "filename": "${workspace}/logs/client/client",
                "pattern": "yyyy-MM-dd.log",
                "alwaysIncludePattern": true,
                "daysToKeep": 30
            }
        },
        "categories": {
            "clientLog": {
                "appenders": [
                    "clientLog",
                    "console"
                ],
                "level": "INFO"
            }
        }
    },
    "koa":{
        proxy:false,                 //是否有代理,默认false
        subdomainOffset:2,        //子域偏移量，默认2
        proxyIpHeader:X-Forwarded-F,          //代理ip头，默认为X-Forwarded-F
        maxIpsCount:0,            //从代理ip头读取的最大ip数，默认为0（表示无穷大）
        env:any,
        keys:any                       //签名cookie密钥
    }
}
```

### 插件使用说明
#### PlusSchedule
计划任务的文件都必须放在schedule目录下，文件结构如下
```
import { ScheduleBase } from "../base/plus/schedule/ScheduleBase";

export class ScheduleEveryDay extends ScheduleBase {
    get config() {
        return {
            cron: "0 0 0 * * *" //每日0点
        };
    }

    protected async mExecute() {
        //这些写计划任务的内容
    }
}
```

#### PlusHttpServer
作为基于http服务插件，所有接口都放在controller下建文件导出对象，文件形式如下
```
import { BaseService } from "../base/plus/httpServer/BaseService";

export class GmServer extends BaseService {
    constructor() {
        super();
        this._regist(this.func,"post", "gmAuthorize");      //post可以是get，最后一个参数可以不传，则接口cmd就是${文件名}/${方法名}
    }

    async func(a1,a2,a3){       //这里的参数key名就是访问接口对象中的key
        //这里写接口逻辑
    }
}
```
对外接口的访问结构如下:
http://serverUrl/cmd
```
a1:xxx,
a2:xx,
a3:xxx
```

返回结构如下:
```
code:number;               //正常为200，其余都是异常
msg:string;                //异常信息描述
data:any;                  //返回内容数据
```


#### PlusSequelize
作为数据库组件，所有model对象都必须放在model目录下，文件结构如下:
```
import { ModelConfigBase } from "../base/plus/sequelize/ModelConfigBase";
import { ModelBase } from "../base/plus/sequelize/ModelBase";
import { DataTypes } from "sequelize";

export class ModelGameConfig extends ModelConfigBase {
    static readonly TABLE_NAME: string = "config_base_game";
    protected mInit() {
        this.mNeedSync = false;
        this.mAddAttr("id", { type: DataTypes.INTEGER, primaryKey: true });
        this.mAddAttr("name", DataTypes.STRING(64));
        this.mAddAttr("status", DataTypes.INTEGER);
        super.mInit();
    }
}

export class ModelGame extends ModelBase {
    id: number;
    name:string;
    status:number
}
```