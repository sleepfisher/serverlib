import { readdirSync, existsSync } from "fs";

export class FileUtils {
    /**获取目录下指定后缀的文件列表 */
    static readFile(path: string, fileType: string): string[] {
        if(!existsSync(path))return [];
        let files = readdirSync(path);
        files = files.filter((f) => {
            return f.endsWith("." + fileType);
        });
        return files;
    }
}