import { Configuration, Logger } from "log4js";
import { EnumLogLevel } from "../entity/IConfig";
var log4js = require('log4js');

//响应日志输出完整路径
const outLogPath = "${workspace}/logs/out/out";
//逻辑异常输出完整路径
const excepitonLogPath = "${workspace}/logs/error/err";


var logConfigs: Configuration = {
    appenders: {
        console: {//记录器1:输出到控制台
            type: 'console',
        },
        access: {
            type: 'dateFile',
            filename: outLogPath,
            pattern: 'yyyy-MM-dd-hh.log',
            alwaysIncludePattern: true,
            daysToKeep: 7                   //其实没效果- -看源码没解析这个参数
        },
        errorFile: {
            type: 'dateFile',
            filename: excepitonLogPath,
            pattern: 'yyyy-MM-dd.log',
            alwaysIncludePattern: true,
            daysToKeep: 30
        },
    },
    categories: {
        default: { appenders: ['access', 'console'], level: EnumLogLevel.INFO },
        errorFile: { appenders: ['errorFile', 'console'], level: EnumLogLevel.ERROR }
    }
}


export class LogUtils {
    private _info: Logger;
    private _errLog: Logger;
    private _logs: { [key: string]: Logger };
    constructor(workspace: string, logPath: string, configs?: Configuration) {
        if(configs){
            if(configs.appenders){
                for(let key in configs.appenders){
                    logConfigs.appenders[key]=configs.appenders[key];
                }
            }
            if(configs.categories){
                for(let key in configs.categories){
                    logConfigs.categories[key]=configs.categories[key];
                }
            }
        }


        for (let key in logConfigs.appenders) {
            let appender: any = logConfigs.appenders[key];
            if (appender.filename) {
                appender.filename = appender.filename.replace(/\$\{workspace\}/g, workspace).replace(/\$\{logspace\}/g, logPath);
            }
        }
        log4js.configure(logConfigs);
        this._logs = {};
        for (let key in logConfigs.categories) {
            this._logs[key] = log4js.getLogger(key);
        }
        this._info = this._logs["default"];
        this._errLog = this._logs["errorFile"];
    }

    updateLogLevel(logLevels: { [key: string]: EnumLogLevel }) {
        if (!logLevels) return;
        for (let key in logLevels) {
            let logger: Logger = this._logs[key];
            if (logger) {
                logger.level = logLevels[key];
            }
        }
    }

    trace(message: any, ...args: any[]) {
        this._info.trace(message, ...args);
    }

    debug(message: any, ...args: any[]): void {
        this._info.debug(message, ...args);
    }

    info(message: any, ...args: any[]): void {
        this._info.info(message, ...args);
    }

    warn(message: any, ...args: any[]): void {
        this._info.warn(message, ...args);
    }

    error(message: any, ...args: any[]): void {
        this._errLog.error(message, ...args);
    }

    fatal(message: any, ...args: any[]): void {
        this._info.fatal(message, ...args);
    }

    getLogger(type:string):Logger{
        return this._logs[type];
    }
}