export class ClsUtils {
    static targetIsCls(target: any, cls: any) {
        if (!Object.getPrototypeOf(target)) {
            return false;
        }
        if (Object.getPrototypeOf(target).toString() !== cls.toString()) {
            return this.targetIsCls(Object.getPrototypeOf(target), cls);
        }
        return true;
    }
}