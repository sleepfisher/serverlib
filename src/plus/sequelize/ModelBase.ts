import { Model } from "sequelize";

export class ModelBase extends Model {
    public getValues() {
        var arr = this["_options"].attributes;
        var obj = {};
        if (arr) {
            for (var i = 0; i < arr.length; i++) {
                obj[arr[i]] = this[arr[i]];
            }
        } else {
            for (var key in this["dataValues"]) {
                obj[key] = this[key];
            }
        }
        return obj;
    }
}