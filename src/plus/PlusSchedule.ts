import { PlusBase } from "./PlusBase";
import { join } from "path";
import { ScheduleBase } from "./schedule/ScheduleBase";
import { ClsUtils } from "../utils/ClsUtils";
import { FileUtils } from "../utils/FileUtils";
export class PlusSchedule extends PlusBase {
    static NAME: string = "schedule";
    private _path: string;
    constructor(path: string) {
        super(PlusSchedule.NAME);
        this._path = path;
    }

    async init() {
        let jsFiles = FileUtils.readFile(this._path, "js");
        for(let i=0;i<jsFiles.length;i++){
            let d=require(join(this._path,jsFiles[i]));
            for(let key in d){
                if(ClsUtils.targetIsCls(d[key],ScheduleBase)){
                    await this.regisSchedule(new d[key]());
                }
            }
        }
        this.mCompleteHandle();
    }

    public async regisSchedule(scheduleItem:ScheduleBase){
        await scheduleItem.start();
    }
}