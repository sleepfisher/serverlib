import { IGlobal } from "../Index";
import { PlusBase } from "./PlusBase";
import { IProtocol } from "./websocket/protocol/IProtocol";
import { SocketClient } from "./websocket/server/SocketClient";
import { WsServer } from "./websocket/server/WsServer";
/**
 * socket服务监听，客户端连接初始化
 */
export class PlusWsServer extends PlusBase {
    static NAME = "PlusWsServer";
    protected _ws:WsServer;
    protected _isProxy:boolean;
    constructor(name:string,port: number, path: string,protocol?:IProtocol,isProxy:boolean=false) {
        super(name);
        this._isProxy=isProxy;
        this._ws=new WsServer(port,path,protocol,isProxy);
    }

    init() {
        this._ws.verifyFunc=this._verifyFunc.bind(this);
        this._ws.on("closeClient",this._onCloseClient.bind(this));
        this._ws.on("message",this._onMessage.bind(this));
        this._ws.init();
        this.mCompleteHandle();
    }


    //重写该方法可以在链接阶段拒绝链接
    protected _verifyFunc(info:any){
        return true;
    }


    public getClient(link: string): SocketClient {
        return this._ws.getClient(link);
    }

    public getClientByKey(key: string): SocketClient {
        return this._ws.getClientByKey(key);
    }

    public closeClient(linkId: string) {
        this._ws.closeClient(linkId);
    }

    public send(linkId: string, data: any) {
        this._ws.send(linkId,data);
    }

    public sendCmd(linkId: string, cmd: string, data: any,id:number=0) {
        this._ws.sendCmd(linkId,cmd,data,id);
    }

    protected _onCloseClient(linkId:string){
        
    }

    protected _onMessage(linkId:string,data:any){

    }
}