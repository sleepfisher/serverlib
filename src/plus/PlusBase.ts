import { EventEmitter } from "events";
import { IGlobal } from "../entity/Global";
declare var global: IGlobal;
export abstract class PlusBase extends EventEmitter {
    private _name: string;
    constructor(name: string) {
        super();
        this._name = name;
    }

    abstract init(): void;

    protected mCompleteHandle() {
        this.emit("complete");
    }

    protected mOnFail(err?: any) {
        this.log.error(`模块[${this._name}]初始化失败`, err);
    }

    get name() {
        return this._name;
    }

    get log() {
        return global.log;
    }
}