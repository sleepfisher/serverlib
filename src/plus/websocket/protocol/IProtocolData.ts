export interface IProtocolData{
    cmd:string;
    id:number;
    data:any;
}