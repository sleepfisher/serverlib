import { IProtocol } from "./IProtocol";
import { IProtocolData } from "./IProtocolData";

export class JsonProtocol implements IProtocol{
    decode(buff:string):IProtocolData{
        if(buff==this.heartData)return null;
        var data=JSON.parse(buff);
        return data;
    }
    encode(cmd:string,data:any,id?:number):Buffer|string{
        return JSON.stringify({cmd:cmd,data:data,id:id});
    }

    get heartData(){return "heart";}
}