import { IProtocolData } from "./IProtocolData";

//约定协议
export interface IProtocol{
    decode(buff:Buffer | string):IProtocolData;
    encode?(cmd:string,data:any,id?:number):Buffer|string;
    encode?(...args):Buffer|string;
}