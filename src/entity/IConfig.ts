import { EventEmitter } from "events";
import { Configuration } from "log4js";

export interface IConfig extends EventEmitter {
    sql: ISqlConfig;
    port: number;
    wsPort: number;
    redis: IRedisOption;
    bMaster: boolean;
    env: string;
    logLevels: { [key: string]: EnumLogLevel },
    logConfig?: Configuration,
    koa?: {
        proxy?: boolean,                 //是否有代理
        subdomainOffset?: number,        //子域偏移量
        proxyIpHeader?: string,          //代理ip头，默认为X-Forwarded-F
        maxIpsCount?: number,            //从代理ip头读取的最大ip数，默认为0（表示无穷大）
        env?: any,
        keys?: any                       //签名cookie密钥
    }
}


export interface ISqlConfig {
    dbName: string,
    user: string,
    pwd: string,
    options: {
        host: string,
        port: number,
        dialect: string         //使用数据库类型，mysql或mssql
    },
}

export interface IRedisOption {
    port: number,
    host: string,
    password?: string,
    db: number,
    cluster?: [{ host: string, port: number }]
}


/**级别按顺序 */
export const enum EnumLogLevel {
    ALL = "ALL",
    TRACE = "TRACE",
    DEBUG = "DEBUG",
    INFO = "INFO",
    WARN = "WARN",
    ERROR = "ERROR",
    FATAL = "FATAL",
    MARK = "MARK",
    OFF = "OFF"
}
