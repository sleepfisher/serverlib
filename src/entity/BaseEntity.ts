export class BaseResp {
    code: number;
    msg: string;
    data: any;
    constructor(code: number, data?: any, msg?: string) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}

export class BaseError extends Error {
    private _code: number;
    constructor(message: string, code: number = 0) {
        super(message)
        this._code = code;
    }

    get code() {
        return this._code;
    }
}