export enum EnumCode {
    SUCCESS = 200,              //成功
    FAIL = 500,                 //失败

    SQL_ERROR = 100001,

    REMOTE_ERROR = 100002,

    PARAM_MISSING = 100003,    //参数缺失
    PARAM_INVALID = 100004,
}