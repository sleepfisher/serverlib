const OpContent = {
    ne: Symbol.for('ne'),
    gte: Symbol.for('gte'),
    gt: Symbol.for('gt'),
    lte: Symbol.for('lte'),
    lt: Symbol.for('lt'),
    notBetween: Symbol.for('notBetween'),
    between: Symbol.for("between")
};

interface IOp {
    /**不等于 */
    readonly ne: unique symbol,
    /**大于等于 */
    readonly gte: unique symbol,
    /**大于 */
    readonly gt: unique symbol,
    /**小于等于*/
    readonly lte: unique symbol,
    /**小于 */
    readonly lt: unique symbol,
    /**不属于这个区间，区间本身也不属于 */
    readonly notBetween: unique symbol,
    /**属于这个区间，包含区间本身 */
    readonly between: unique symbol;
}

export const Op: IOp = <any>OpContent;